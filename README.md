# plaintxt's dotfiles
- This repository holds the dotfiles for programs I use on a regular basis.
- You're free to use them however you like, it are just configuration files after all.

## Installation
- Clone the repository to your home directory.
- Run `install` to create symbolic links to the configuration files. The `install` script provides several arguments for splitting the installation process.
 
## Vim plugins
Vim version v8.1.2269 or higher is needed for YouCompleteMe to work because vim needs to be compiled with Python 3.6 support. On Ubuntu 20.04 or higher `vim-nox` can be installed via apt to fulfill this requirement.

- You'll have to run `:PluginInstall` in Vim to let Vim-Plug install all the listed Vim plugins.
- Install dependencies for YouCompleteMe, following the original documentation.

### Xorg
- `xorg.conf`, which can be placed in `/etc/X11/` to make the MadCatz R.A.T. 7 work properly.
