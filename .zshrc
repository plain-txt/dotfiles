########################################
# $HOME/.zshrc
########################################

# Set up the prompt
autoload -Uz promptinit
promptinit
PROMPT="%B%F{174}%1n%f%b"
PROMPT+="%B%F{172}@%f%b"
PROMPT+="%B%F{179}%1m:%f%b"
PROMPT+="%B%F{blue}%~%f%#%b "

setopt histignorealldups sharehistory

# Keep 1000 lines of history within the shell and save 2000 to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=2000
HISTFILE=~/.zsh_history

# Enable lessfile for opening non-text files with less
eval "$(lessfile)"

# Source aliases if present
if [ -f $HOME/.aliases ]; then
    . $HOME/.aliases
fi

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'
