""""""""""""""""""""""""""""""""""""""""""""
" .vimrc 
""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-plug plugin manager
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set nocompatible                        " Make vim less Vi-compatible
filetype plugin on                      " Required for several plugins
syntax on                               " Required for several plugins

call plug#begin()
Plug 'ycm-core/YouCompleteMe'
Plug 'mattn/emmet-vim'
Plug 'vimwiki/vimwiki'
Plug 'junegunn/seoul256.vim'
Plug 'itchyny/lightline.vim'
Plug 'preservim/nerdtree'
Plug 'airblade/vim-gitgutter'
call plug#end()                         " required

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Basic changes
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

set wildmode=longest,list,full          " Enable autocompletion for commands

set tabstop=4                           " Tab indents with 4 spaces
set shiftwidth=4                        " When indenting with >, use 4 spaces
set expandtab                           " On pressing tab, insert 4 spaces
set number                              " Set line numbers
set numberwidth=3                       " Set width for line numbers

set mouse=nicr                          " Enable mouse scrolling

set splitbelow splitright               " Split to bottom and right

" Change split navigation bindings
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Plugin settings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Seoul256 theme
let g:seoul256_background = 236
colo seoul256

" Vimwiki
let g:vimwiki_list = [{'path': '~/Vimwiki/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]
" Lightline
set laststatus=2
let g:lightline = {'colorscheme':'seoul256'}
" NERDtree
"Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in")
            \| wincmd p | endif

" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 &&
            \exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
