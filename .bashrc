########################################
# $HOME/.bashrc
########################################

# Change prompt
PSUSER='\[\e[1;38;5;174m\]\u'
PSAT='\[\e[1;38;5;172m\]@'
PSHOST='\[\e[1;38;5;179m\]\h:'
PSDIR='\[\e[1;38;5;117m\]\w'
PS1=$PSUSER$PSAT$PSHOST$PSDIR'\[\e[0m\]\$ '
PS2='\[\e[2;38;5;178m\]> '

# don't add duplicate lines or lines starting with a space
HISTCONTROL=ignoreboth

# set .bash_history filesize and max number of commands to remember
HISTSIZE=1000
HISTFILESIZE=2000

# append to .bash_history instead of overwriting it
shopt -s histappend

# Update number of lines and columns based on window size
shopt -s checkwinsize

# enable lessfile for opening non-text files with less
eval "$(lessfile)"

# Source aliases if present
if [ -f $HOME/.aliases ]; then
    . $HOME/.aliases
fi
# Source bash completion if present
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

